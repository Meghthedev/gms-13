/*
 * Copyright (C) 2022 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.android.emergency;

import static com.android.emergency.RestoreProviderContract.AUTHORITY;
import static com.android.emergency.RestoreProviderContract.MEDICAL_INFO_URI_PATH;
import static com.android.emergency.RestoreProviderContract.EMERGENCY_CONTACT_URI_PATH;
import static com.android.emergency.RestoreProviderContract.MEDICAL_INFO;
import static com.android.emergency.RestoreProviderContract.EMERGENCY_CONTACT;
import static com.android.emergency.RestoreProviderContract.QUOTE_CONTACT_SEPARATOR;
import static com.android.emergency.RestoreProviderContract.CHECK_MIGRATION_STATE;
import static com.android.emergency.RestoreProviderContract.SET_MIGRATION_COMPLETED;
import static com.android.emergency.RestoreProviderContract.KEY_MIGRATION_STATE;
import static com.android.emergency.RestoreProviderContract.KEY_EMERGENCY_CONTACTS;
import static com.android.emergency.RestoreProviderContract.KEY_ADDRESS;
import static com.android.emergency.RestoreProviderContract.KEY_BLOOD_TYPE;
import static com.android.emergency.RestoreProviderContract.KEY_ALLERGIES;
import static com.android.emergency.RestoreProviderContract.KEY_MEDICATIONS;
import static com.android.emergency.RestoreProviderContract.KEY_MEDICAL_CONDITIONS;
import static com.android.emergency.RestoreProviderContract.KEY_ORGAN_DONOR;
import static com.android.emergency.RestoreProviderContract.KEYS_EDIT_EMERGENCY_INFO;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.UriMatcher;
import android.content.pm.ProviderInfo;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;

import com.android.internal.annotations.VisibleForTesting;

public class RestoreProvider extends ContentProvider {
    private UriMatcher uriMatcher;

    @Override
    public final void attachInfo(Context context, ProviderInfo info) {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(AUTHORITY, MEDICAL_INFO_URI_PATH, MEDICAL_INFO);
        uriMatcher.addURI(AUTHORITY, EMERGENCY_CONTACT_URI_PATH, EMERGENCY_CONTACT);

        if (!info.exported) {
            throw new SecurityException("Provider must be exported");
        }
        if (!info.grantUriPermissions) {
            throw new SecurityException("Provider must grantUriPermissions");
        }

        super.attachInfo(context, info);
    }

    @Override
    public boolean onCreate() {
        return true;
    }

    @Override
    public final Cursor query(Uri uri, String[] projection, String selection,
            String[] selectionArgs,
            String sortOrder) {
        verifyPermission();
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(
                getContext());
        switch (uriMatcher.match(uri)) {
            case MEDICAL_INFO:
                return getMedicalInfoCursor(sharedPreferences);
            case EMERGENCY_CONTACT:
                return getEmergencyContactCursor(sharedPreferences);
        }
        throw new IllegalArgumentException("Unknown Uri: " + uri);
    }

    @Override
    public final String getType(Uri uri) {
        throw new UnsupportedOperationException("Get type not supported");
    }

    @Override
    public final Uri insert(Uri uri, ContentValues values) {
        throw new UnsupportedOperationException("Insert not supported");
    }

    @Override
    public final int delete(Uri uri, String selection, String[] selectionArgs) {
        throw new UnsupportedOperationException("Delete not supported");
    }

    @Override
    public final int update(Uri uri, ContentValues values, String selection,
            String[] selectionArgs) {
        throw new UnsupportedOperationException("Update not supported");
    }

    @Override
    public final Bundle call(String method, String arg, Bundle extras) {
        verifyPermission();
        switch (method) {
            case SET_MIGRATION_COMPLETED:
                return setMigrationCompleted();
            case CHECK_MIGRATION_STATE:
                return checkMigrationState();
        }
        throw new IllegalArgumentException("Unknown method: " + method);
    }

    private MatrixCursor getMedicalInfoCursor(SharedPreferences sharedPreferences) {
        MatrixCursor matrixCursor = new MatrixCursor(
                KEYS_EDIT_EMERGENCY_INFO);
        matrixCursor.newRow()
                .add(KEY_ADDRESS, sharedPreferences.getString(KEY_ADDRESS, ""))
                .add(KEY_BLOOD_TYPE, sharedPreferences.getString(KEY_BLOOD_TYPE, ""))
                .add(KEY_ALLERGIES, sharedPreferences.getString(KEY_ALLERGIES, ""))
                .add(KEY_MEDICATIONS, sharedPreferences.getString(KEY_MEDICATIONS, ""))
                .add(KEY_MEDICAL_CONDITIONS, sharedPreferences.getString(KEY_MEDICAL_CONDITIONS,
                        ""))
                .add(KEY_ORGAN_DONOR, sharedPreferences.getString(KEY_ORGAN_DONOR, ""));
        return matrixCursor;
    }

    private MatrixCursor getEmergencyContactCursor(SharedPreferences sharedPreferences) {
        String[] emergencyContactsColumns = {KEY_EMERGENCY_CONTACTS};
        MatrixCursor matrixCursor = new MatrixCursor(emergencyContactsColumns);
        String[] emergencyContactsArray = deserialize(
                sharedPreferences.getString(KEY_EMERGENCY_CONTACTS, ""));
        for (String emergencyContact : emergencyContactsArray) {
            matrixCursor.newRow().add(KEY_EMERGENCY_CONTACTS, emergencyContact);
        }
        return matrixCursor;
    }

    private String[] deserialize(String emergencyContacts) {
        return emergencyContacts.split(QUOTE_CONTACT_SEPARATOR);
    }

    private Bundle setMigrationCompleted() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(
                getContext());
        sharedPreferences.edit().putBoolean(KEY_MIGRATION_STATE, true).commit();
        return null;
    }

    private Bundle checkMigrationState() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(
                getContext());
        Bundle bundle = new Bundle();
        bundle.putBoolean(CHECK_MIGRATION_STATE,
                sharedPreferences.getBoolean(KEY_MIGRATION_STATE, false));
        return bundle;
    }

    /**
     * Checks the permission of the caller. This throws a {@link SecurityException} when the
     * calling package is not signed by a Google certificate.
     */
    @VisibleForTesting
    void verifyPermission() throws SecurityException {
        Context context = getContext();
        String callingPackage = getCallingPackage();
        boolean isCallingPackageAllowlisted = SignatureVerifier.isCallerAllowlisted(context,
                callingPackage);

        if (!isCallingPackageAllowlisted) {
            throw new SecurityException("Illegal access by package: " + callingPackage);
        }
    }
}

